wtdata({
    "matches": {
        "timeNow": 1436534222,
        "match": [{
            "time": null,
            "MatchHour2": "17:06",
            "ResumenTransmision": "",
            "MXvideo": "",
            "MatchGuid": "4a1f8913-d80d-11e4-b192-e5cb4a0e6bed",
            "Website": "http://deportes.televisa.com/mxm/futbol/partidos/copa-oro-2015/30068/honduras-vs-panama/dispatcher/",
            "USAvideo": "",
            "MatchDate": "10/07/2015",
            "TimeStamp": 1436547960,
            "period": "P",
            "periodabrev": "",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "Panam\u00e1",
                    "club": "Rep\u00fablica de Panam\u00e1",
                    "guid": "17abb716-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                },
                "local": {
                    "penals": "",
                    "name": "Honduras",
                    "club": "Honduras",
                    "guid": "24ae95aa-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                }
            },
            "MatchId": "30068",
            "IdTournament": "422",
            "txtVideo": "",
            "txtLink": "Previo &raquo;",
            "MatchHour": "17:06:00",
            "EventTournamentName": "Copa Oro 2015"
        }, {
            "time": null,
            "MatchHour2": "19:36",
            "ResumenTransmision": "",
            "MXvideo": "",
            "MatchGuid": "09b71afd-cb47-11e4-b192-e5cb4a0e6bed",
            "Website": "http://deportes.televisa.com/mxm/futbol/partidos/copa-oro-2015/30021/estados-unidos-vs-haiti/dispatcher/",
            "USAvideo": "",
            "MatchDate": "10/07/2015",
            "TimeStamp": 1436556960,
            "period": "P",
            "periodabrev": "",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "Hait\u00ed",
                    "club": "Republique d'Haiti",
                    "guid": "1838cfc0-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                },
                "local": {
                    "penals": "",
                    "name": "Estados Unidos",
                    "club": "Estados Unidos",
                    "guid": "ff2caf10-a105-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                }
            },
            "MatchId": "30021",
            "txtVideo": "",
            "txtLink": "Previo &raquo;",
            "MatchHour": "19:36:00",
            "EventTournamentName": "Copa Oro 2015"
        }, {
            "time": null,
            "MatchHour2": "10:00",
            "ResumenTransmision": "",
            "MXvideo": "",
            "MatchGuid": "187bf38b-eb97-11e4-b192-e5cb4a0e6bed",
            "Website": "",
            "USAvideo": "",
            "MatchDate": "11/07/2015",
            "TimeStamp": 1436608800,
            "period": "P",
            "periodabrev": "",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "Colombia",
                    "club": "Colombia",
                    "guid": "fdea270e-a105-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                },
                "local": {
                    "penals": "",
                    "name": "M\u00e9xico",
                    "club": "M\u00e9xico",
                    "guid": "18805b74-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                }
            },
            "MatchId": "30160",
            "txtVideo": "",
            "txtLink": "",
            "MatchHour": "10:00:00",
            "EventTournamentName": "Panamericanos Toronto 2015. Femenil"
        }, {
            "time": null,
            "MatchHour2": "17:36",
            "ResumenTransmision": "",
            "MXvideo": "",
            "MatchGuid": "2245f3f4-cb47-11e4-b192-e5cb4a0e6bed",
            "Website": "http://deportes.televisa.com/mxm/futbol/partidos/copa-oro-2015/30022/jamaica-vs-canada/dispatcher/",
            "USAvideo": "",
            "MatchDate": "11/07/2015",
            "TimeStamp": 1436636160,
            "period": "P",
            "periodabrev": "",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "Canad\u00e1",
                    "club": "Canada",
                    "guid": "253b88f2-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                },
                "local": {
                    "penals": "",
                    "name": "Jamaica",
                    "club": "Jamaica",
                    "guid": "24f55512-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                }
            },
            "MatchId": "30022",
            "txtVideo": "",
            "txtLink": "Previo &raquo;",
            "MatchHour": "17:36:00",
            "EventTournamentName": "Copa Oro 2015"
        }, {
            "time": null,
            "MatchHour2": "20:15",
            "ResumenTransmision": "",
            "MXvideo": "",
            "MatchGuid": "3f7cf778-cb47-11e4-b192-e5cb4a0e6bed",
            "Website": "http://deportes.televisa.com/mxm/futbol/partidos/copa-oro-2015/30023/costa-rica-vs-el-salvador/dispatcher/",
            "USAvideo": "",
            "MatchDate": "11/07/2015",
            "TimeStamp": 1436645700,
            "period": "P",
            "periodabrev": "",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "El Salvador",
                    "club": "Rep\u00fablica de El Salvador",
                    "guid": "25c80e94-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                },
                "local": {
                    "penals": "",
                    "name": "Costa Rica",
                    "club": "Costa Rica",
                    "guid": "2581fdbe-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                }
            },
            "MatchId": "30023",
            "txtVideo": "",
            "txtLink": "Previo &raquo;",
            "MatchHour": "20:15:00",
            "EventTournamentName": "Copa Oro 2015"
        }, {
            "time": null,
            "MatchHour2": "22:30",
            "ResumenTransmision": "",
            "MXvideo": "",
            "MatchGuid": "48ae60d5-15f5-11e5-b192-e5cb4a0e6bed",
            "Website": "",
            "USAvideo": "",
            "MatchDate": "11/07/2015",
            "TimeStamp": 1436653800,
            "period": "P",
            "periodabrev": "",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "Am\u00e9rica",
                    "club": "Club Am\u00e9rica",
                    "guid": "facb3266-a105-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                },
                "local": {
                    "penals": "",
                    "name": "LA Galaxy",
                    "club": "LA Galaxy",
                    "guid": "2475fe44-e975-102d-95e7-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                }
            },
            "MatchId": "30353",
            "txtVideo": "",
            "txtLink": "",
            "MatchHour": "22:30:00",
            "EventTournamentName": "International Champions Cup 2015"
        }, {
            "time": null,
            "MatchHour2": "17:36",
            "ResumenTransmision": "",
            "MXvideo": "",
            "MatchGuid": "624a2f2a-cb47-11e4-b192-e5cb4a0e6bed",
            "Website": "",
            "USAvideo": "",
            "MatchDate": "12/07/2015",
            "TimeStamp": 1436722560,
            "period": "P",
            "periodabrev": "",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "Cuba",
                    "club": "Rep\u00fablica de Cuba",
                    "guid": "170f536c-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                },
                "local": {
                    "penals": "",
                    "name": "Trinidad y Tobago",
                    "club": "Trinidad and Tobago Football Federation",
                    "guid": "fefcd128-a105-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                }
            },
            "MatchId": "30024",
            "txtVideo": "",
            "txtLink": "",
            "MatchHour": "17:36:00",
            "EventTournamentName": "Copa Oro 2015"
        }, {
            "time": null,
            "MatchHour2": "20:06",
            "ResumenTransmision": "",
            "MXvideo": "",
            "MatchGuid": "7b64cdc0-cb47-11e4-b192-e5cb4a0e6bed",
            "Website": "",
            "USAvideo": "",
            "MatchDate": "12/07/2015",
            "TimeStamp": 1436731560,
            "period": "P",
            "periodabrev": "",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "M\u00e9xico",
                    "club": "M\u00e9xico",
                    "guid": "18805b74-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                },
                "local": {
                    "penals": "",
                    "name": "Guatemala",
                    "club": "Rep\u00fablica de Guatemala",
                    "guid": "18c690b2-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                }
            },
            "MatchId": "30025",
            "txtVideo": "",
            "txtLink": "",
            "MatchHour": "20:06:00",
            "EventTournamentName": "Copa Oro 2015"
        }, {
            "time": null,
            "MatchHour2": "19:30",
            "ResumenTransmision": "",
            "MXvideo": "",
            "MatchGuid": "2b1867b8-eb99-11e4-b192-e5cb4a0e6bed",
            "Website": "",
            "USAvideo": "",
            "MatchDate": "13/07/2015",
            "TimeStamp": 1436815800,
            "period": "P",
            "periodabrev": "",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "M\u00e9xico",
                    "club": "M\u00e9xico",
                    "guid": "18805b74-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                },
                "local": {
                    "penals": "",
                    "name": "Paraguay",
                    "club": "Asociaci\u00f3n Paraguaya de Futbol",
                    "guid": "fdca0f6e-a105-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                }
            },
            "MatchId": "30175",
            "txtVideo": "",
            "txtLink": "",
            "MatchHour": "19:30:00",
            "EventTournamentName": "Panamericanos Toronto 2015"
        }, {
            "time": null,
            "MatchHour2": "18:06",
            "ResumenTransmision": "",
            "MXvideo": "",
            "MatchGuid": "70190a4f-d80d-11e4-b192-e5cb4a0e6bed",
            "Website": "",
            "USAvideo": "",
            "MatchDate": "13/07/2015",
            "TimeStamp": 1436810760,
            "period": "P",
            "periodabrev": "",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "Honduras",
                    "club": "Honduras",
                    "guid": "24ae95aa-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                },
                "local": {
                    "penals": "",
                    "name": "Hait\u00ed",
                    "club": "Republique d'Haiti",
                    "guid": "1838cfc0-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                }
            },
            "MatchId": "30069",
            "txtVideo": "",
            "txtLink": "",
            "MatchHour": "18:06:00",
            "EventTournamentName": "Copa Oro 2015"
        }, {
            "time": null,
            "MatchHour2": "20:45",
            "ResumenTransmision": "",
            "MXvideo": "",
            "MatchGuid": "a40d5cfa-cb47-11e4-b192-e5cb4a0e6bed",
            "Website": "",
            "USAvideo": "",
            "MatchDate": "13/07/2015",
            "TimeStamp": 1436820300,
            "period": "P",
            "periodabrev": "",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "Estados Unidos",
                    "club": "Estados Unidos",
                    "guid": "ff2caf10-a105-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                },
                "local": {
                    "penals": "",
                    "name": "Panam\u00e1",
                    "club": "Rep\u00fablica de Panam\u00e1",
                    "guid": "17abb716-a106-102c-8c37-0019b9d72a1e",
                    "goals": "-",
                    "result": "NO JUGADO"
                }
            },
            "MatchId": "30026",
            "txtVideo": "",
            "txtLink": "",
            "MatchHour": "20:45:00",
            "EventTournamentName": "Copa Oro 2015"
        }, {
            "time": null,
            "MatchHour2": "18:06",
            "ResumenTransmision": "http://deportes.televisa.com/video/copa-oro/2015-07-07/resumen-panama-vs-haiti/",
            "MXvideo": "http://deportes.televisa.com/video/copa-oro/2015-07-07/resumen-panama-vs-haiti/@@@Resumen",
            "MatchGuid": "796dc115-cb46-11e4-b192-e5cb4a0e6bed",
            "Website": "http://deportes.televisa.com/mxm/futbol/partidos/copa-oro-2015/30016/panama-vs-haiti/dispatcher/",
            "USAvideo": "",
            "MatchDate": "07/07/2015",
            "TimeStamp": 1436292360,
            "period": "F",
            "periodabrev": "FIN",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "Hait\u00ed",
                    "club": "Republique d'Haiti",
                    "guid": "1838cfc0-a106-102c-8c37-0019b9d72a1e",
                    "goals": "1",
                    "result": "EMPATADO"
                },
                "local": {
                    "penals": "",
                    "name": "Panam\u00e1",
                    "club": "Rep\u00fablica de Panam\u00e1",
                    "guid": "17abb716-a106-102c-8c37-0019b9d72a1e",
                    "goals": "1",
                    "result": "EMPATADO"
                }
            },
            "MatchId": "30016",
            "txtVideo": "",
            "txtLink": "Cr&oacute;nica &raquo;",
            "MatchHour": "18:06:00",
            "EventTournamentName": "Copa Oro 2015"
        }, {
            "time": null,
            "MatchHour2": "20:36",
            "ResumenTransmision": "http://deportes.televisa.com/video/copa-oro/2015-07-07/resumen-eua-vs-honduras/",
            "MXvideo": "http://deportes.televisa.com/video/copa-oro/2015-07-07/resumen-eua-vs-honduras/@@@Resumen",
            "MatchGuid": "1b306c6a-d80d-11e4-b192-e5cb4a0e6bed",
            "Website": "http://deportes.televisa.com/mxm/futbol/partidos/copa-oro-2015/30067/estados-unidos-vs-honduras/dispatcher/",
            "USAvideo": "",
            "MatchDate": "07/07/2015",
            "TimeStamp": 1436301360,
            "period": "F",
            "periodabrev": "FIN",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "Honduras",
                    "club": "Honduras",
                    "guid": "24ae95aa-a106-102c-8c37-0019b9d72a1e",
                    "goals": "1",
                    "result": "PERDIDO"
                },
                "local": {
                    "penals": "",
                    "name": "Estados Unidos",
                    "club": "Estados Unidos",
                    "guid": "ff2caf10-a105-102c-8c37-0019b9d72a1e",
                    "goals": "2",
                    "result": "GANADO"
                }
            },
            "MatchId": "30067",
            "IdTournament": "422",
            "txtVideo": "",
            "txtLink": "Cr&oacute;nica &raquo;",
            "MatchHour": "20:36:00",
            "EventTournamentName": "Copa Oro 2015"
        }, {
            "time": null,
            "MatchHour2": "19:06",
            "ResumenTransmision": "http://deportes.televisa.com/video/copa-oro/2015-07-08/resumen-costa-rica-vs-jamaica/",
            "MXvideo": "http://deportes.televisa.com/video/copa-oro/2015-07-08/resumen-costa-rica-vs-jamaica/@@@Resumen",
            "MatchGuid": "908e8efd-cb46-11e4-b192-e5cb4a0e6bed",
            "Website": "http://deportes.televisa.com/mxm/futbol/partidos/copa-oro-2015/30017/costa-rica-vs-jamaica/dispatcher/",
            "USAvideo": "",
            "MatchDate": "08/07/2015",
            "TimeStamp": 1436382360,
            "period": "F",
            "periodabrev": "FIN",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "Jamaica",
                    "club": "Jamaica",
                    "guid": "24f55512-a106-102c-8c37-0019b9d72a1e",
                    "goals": "2",
                    "result": "EMPATADO"
                },
                "local": {
                    "penals": "",
                    "name": "Costa Rica",
                    "club": "Costa Rica",
                    "guid": "2581fdbe-a106-102c-8c37-0019b9d72a1e",
                    "goals": "2",
                    "result": "EMPATADO"
                }
            },
            "MatchId": "30017",
            "IdTournament": "422",
            "txtVideo": "",
            "txtLink": "Cr&oacute;nica &raquo;",
            "MatchHour": "19:06:00",
            "EventTournamentName": "Copa Oro 2015"
        }, {
            "time": null,
            "MatchHour2": "21:36",
            "ResumenTransmision": "http://deportes.televisa.com/video/copa-oro/2015-07-08/resumen-el-salvador-vs-canada/",
            "MXvideo": "http://deportes.televisa.com/video/copa-oro/2015-07-08/resumen-el-salvador-vs-canada/@@@Resumen",
            "MatchGuid": "b353a5f3-cb46-11e4-b192-e5cb4a0e6bed",
            "Website": "http://deportes.televisa.com/mxm/futbol/partidos/copa-oro-2015/30018/el-salvador-vs-canada/dispatcher/",
            "USAvideo": "",
            "MatchDate": "08/07/2015",
            "TimeStamp": 1436391360,
            "period": "F",
            "periodabrev": "FIN",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "Canad\u00e1",
                    "club": "Canada",
                    "guid": "253b88f2-a106-102c-8c37-0019b9d72a1e",
                    "goals": "0",
                    "result": "EMPATADO"
                },
                "local": {
                    "penals": "",
                    "name": "El Salvador",
                    "club": "Rep\u00fablica de El Salvador",
                    "guid": "25c80e94-a106-102c-8c37-0019b9d72a1e",
                    "goals": "0",
                    "result": "EMPATADO"
                }
            },
            "MatchId": "30018",
            "IdTournament": "422",
            "txtVideo": "",
            "txtLink": "Cr&oacute;nica &raquo;",
            "MatchHour": "21:36:00",
            "EventTournamentName": "Copa Oro 2015"
        }, {
            "time": null,
            "MatchHour2": "18:06",
            "ResumenTransmision": "http://deportes.televisa.com/video/copa-oro/2015-07-09/resumen-trinidad-tobago-vs-guatemala/",
            "MXvideo": "http://deportes.televisa.com/video/copa-oro/2015-07-09/resumen-trinidad-tobago-vs-guatemala/@@@Resumen",
            "MatchGuid": "cc1b7ded-cb46-11e4-b192-e5cb4a0e6bed",
            "Website": "http://deportes.televisa.com/mxm/futbol/partidos/copa-oro-2015/30019/trinidad-y-tobago-vs-guatemala/dispatcher/",
            "USAvideo": "",
            "MatchDate": "09/07/2015",
            "TimeStamp": 1436465160,
            "period": "F",
            "periodabrev": "FIN",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "Guatemala",
                    "club": "Rep\u00fablica de Guatemala",
                    "guid": "18c690b2-a106-102c-8c37-0019b9d72a1e",
                    "goals": "1",
                    "result": "PERDIDO"
                },
                "local": {
                    "penals": "",
                    "name": "Trinidad y Tobago",
                    "club": "Trinidad and Tobago Football Federation",
                    "guid": "fefcd128-a105-102c-8c37-0019b9d72a1e",
                    "goals": "3",
                    "result": "GANADO"
                }
            },
            "MatchId": "30019",
            "IdTournament": "422",
            "txtVideo": "",
            "txtLink": "Cr&oacute;nica &raquo;",
            "MatchHour": "18:06:00",
            "EventTournamentName": "Copa Oro 2015"
        }, {
            "time": null,
            "MatchHour2": "20:45",
            "ResumenTransmision": "http://deportes.televisa.com/video/copa-oro/2015-07-10/resumen-mexico-vs-cuba/",
            "MXvideo": "http://deportes.televisa.com/video/copa-oro/2015-07-10/resumen-mexico-vs-cuba/@@@Resumen",
            "MatchGuid": "e4cf3d28-cb46-11e4-b192-e5cb4a0e6bed",
            "Website": "http://deportes.televisa.com/mxm/futbol/partidos/copa-oro-2015/30020/mexico-vs-cuba/dispatcher/",
            "USAvideo": "",
            "MatchDate": "09/07/2015",
            "TimeStamp": 1436474700,
            "period": "F",
            "periodabrev": "FIN",
            "equipos": {
                "visit": {
                    "penals": "",
                    "name": "Cuba",
                    "club": "Rep\u00fablica de Cuba",
                    "guid": "170f536c-a106-102c-8c37-0019b9d72a1e",
                    "goals": "0",
                    "result": "PERDIDO"
                },
                "local": {
                    "penals": "",
                    "name": "M\u00e9xico",
                    "club": "M\u00e9xico",
                    "guid": "18805b74-a106-102c-8c37-0019b9d72a1e",
                    "goals": "6",
                    "result": "GANADO"
                }
            },
            "MatchId": "30020",
            "txtVideo": "",
            "txtLink": "Cr&oacute;nica &raquo;",
            "MatchHour": "20:45:00",
            "EventTournamentName": "Copa Oro 2015"
        }]
    }
})