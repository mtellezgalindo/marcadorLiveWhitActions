/*
Desarrollado por Martín Téllez Galindo
email: martin.tellez@televisatim.com
*/
$(document).ready(function() {
    readurl();
});
setInterval(function() {
      readurl();
}, 10000);
function readurl(){
//var url = 'http://deportes.televisa.com/content/televisa/deportes/video/futbol-mexicano.video.js';
var local;
var link;
var titulo;
var descripcion;
//se declara varible "local" para despues ser comparada con el localstorage
var url = 'futbol-mexicano.video.js';
    $.ajax({
         type: "GET",
         url: url,
         dataType: "json",
         success: function(response) { 
            //parseo de la url
            var ids = new Array();
            //Se limpia este div con clase wrapp para que cada llamada no se este replicando el contenido       
            $('.wrapp').html('');
             $(response).each(function() {
                 $(this.category).each(function() {
                     $(this.program).each(function() {
                         $(this.videos).each(function(i) {
                             $('.wrapp').append('<div class="titulo"><a href= "'+ this.urlpublic +'" target="_blank"><img src= "' + this.thumb + '"/> <div class="des">' + this.title + '</div></a><div class="description">'+ this.description +'</div></div>');                             
                                ids[i] = this.id;
                                if (i == 0) {
                                    //se asigna el valor de id a la variable "local" para localstorage
                                    local = this.id;
                                    link = this.urlpublic;
                                    titulo = this.title;
                                    titulo = titulo.slice(0,45)+'...';
                                    descripcion = this.description;
                                    cortdescr = descripcion.slice(0,100)+'...';
                                };                                
                                if (i==14) {
                                    return false;
                                };
                         });

                     });
                });
             });
//se verifica si esta declarado el localstorage si no existe se crea
            if(typeof localStorage.idVideoLocal2 == 'undefined'){
                //console.log('no existe');
                localStorage.idVideoLocal2 = local;
                //console.log(localStorage.idVideoLocal2);
            }else{
//si ya existe se crea la variable storageFinal para asignarle el localstorage definido y despues se compara con la 
//variable local para mandar el mensaje
                //console.log('existe');
                //va bien
                var storageFinal = localStorage.idVideoLocal2;
                /*console.log(storageFinal+'-----storageFinal');
                console.log(local+'-----feed');*/
                if (storageFinal == local) {
                    //si son iguales no se manda la notificacion
                    //console.log('no se ha actualizado');
                }else{
                    localStorage.idVideoLocal2 = local;
                    /*console.log(localStorage.idVideoLocal2);
                    console.log('se actulizo');*/
                    show(link,titulo,cortdescr);
                };
            }
         },
    });
}

//funcion show es la que genera el mensaje.
function show(link,titulo,descripcion) {
  var notification = new Notification(titulo, {icon: 'images/icon48.jpg', body: descripcion});
  notification.onclick = function() {
    window.open(link);
  };
}


+